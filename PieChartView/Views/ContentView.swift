import SwiftUI

struct ContentView: View {
    var pieFrom: ([Double]) -> PieChart = bakePie
    
    var body: some View {
        NavigationView {
            VStack {
                pieFrom([25, 25, 20, 12, 18])
                    .frame(width: 300)
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            ContentView().previewDevice("iPhone X").environment(\.colorScheme, .dark)
            ContentView().previewDevice("iPhone X").environment(\.colorScheme, .light)
        }
    }
}
