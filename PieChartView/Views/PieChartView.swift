import SwiftUI

struct PieChart: View {
    let chef: PieRecipeProviding
    
    var body: some View {
        GeometryReader { gr in
            ZStack {
                ForEach(self.chef.sliceRecipes(in: gr.frame(in: .local)), id: \.viewStartAngle) {recipe in
                    PieSlice(recipe: recipe)
                }
            }
        }
    }
}

struct PieSlice: View {
    let recipe: SliceRecipeProviding
    
    private var path: Path {
        var path = Path()
        path.addArc(
            center: recipe.centerPoint,
            radius: recipe.radius,
            startAngle: .degrees(recipe.viewStartAngle),
            endAngle: .degrees(recipe.viewEndAngle),
            clockwise: recipe.viewClockwise
        )
        path.addLine(to: recipe.centerPoint)
        path.closeSubpath()
        return path
    }
    
    var body: some View {
        ZStack {
            path
                .fill()
                .foregroundColor(recipe.color)
                .overlay(path.stroke(Color("overlay"), lineWidth: 5))
            
            Text("\(recipe.percentage, specifier: "%g")%")
                .fontWeight(.bold)
                .offset(recipe.labelOffset)
        }
    }
}
