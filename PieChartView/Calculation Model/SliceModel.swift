import SwiftUI

protocol SliceRecipeProviding {
    var viewStartAngle: Double { get }
    var viewEndAngle: Double { get }
    var viewClockwise: Bool { get }
    var centerPoint: CGPoint { get }
    var radius: CGFloat { get }
    var percentage: Double { get }
    var color: Color { get }
    var labelOffset: CGSize { get }
}

struct SliceRecipe: SliceRecipeProviding {
    var viewStartAngle: Double { startAngle - 90 }
    var viewEndAngle: Double { endAngle - 90 }
    var labelOffset: CGSize { offsetCalc.offset }
    let viewClockwise: Bool
    let centerPoint: CGPoint
    let radius: CGFloat
    let color: Color
    let percentage: Double
    
    private let startAngle: Double
    private let endAngle: Double
    private let offsetCalc: LabelOffsetCalculator
    
    init(startAngle: Double, endAngle: Double, centerPoint: CGPoint, radius: CGFloat, percentage: Double, clockwise: Bool = true, color: Color) {
        self.startAngle = startAngle
        self.endAngle = endAngle
        self.centerPoint = centerPoint
        self.radius = radius
        self.percentage = percentage
        viewClockwise = !clockwise
        offsetCalc = LabelOffsetCalculator(arcRadius: Double(radius), arcStartAngle: startAngle, arcEndAngle: endAngle)
        self.color = color
    }
    
}

struct LabelOffsetCalculator {
    let arcRadius: Double
    let arcStartAngle: Double
    let arcEndAngle: Double
    
    var offset: CGSize {
        let radius = 0.6 * arcRadius
        let ppAngle = ((arcEndAngle - arcStartAngle) / 2) + arcStartAngle
        
        var xOffset = 0.0
        var yOffset = 0.0
        
        if ppAngle <= 90 {
            let alpha = 90 - ppAngle
            xOffset = cos(alpha * Double.pi / 180) * radius
            yOffset = -(sin(alpha * Double.pi / 180) * radius)
            
        } else if (ppAngle > 90) && (ppAngle <= 180) {
            let alpha = 180 - ppAngle
            xOffset = sin(alpha * Double.pi / 180) * radius
            yOffset = cos(alpha * Double.pi / 180) * radius
            
        } else if (ppAngle > 180) && (ppAngle <= 270) {
            let alpha = 270 - ppAngle
            xOffset = -(cos(alpha * Double.pi / 180) * radius)
            yOffset = sin(alpha * Double.pi / 180) * radius
            
        } else if ppAngle > 270 {
            let alpha = 360 - ppAngle
            xOffset = -(sin(alpha * Double.pi / 180) * radius)
            yOffset = -(cos(alpha * Double.pi / 180) * radius)
        }
        return CGSize(width: xOffset, height: yOffset)
    }
}

