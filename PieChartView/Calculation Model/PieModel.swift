import SwiftUI

protocol PieRecipeProviding {
    func sliceRecipes(in frame: CGRect) -> [SliceRecipeProviding]
}

struct PieChef: PieRecipeProviding {
    let percentages: [Double]

    private let colors = [Color(#colorLiteral(red: 0.4620226622, green: 0.8382837176, blue: 1, alpha: 1)), Color(#colorLiteral(red: 0.476841867, green: 0.5048075914, blue: 1, alpha: 1)), Color(#colorLiteral(red: 1, green: 0.1491314173, blue: 0, alpha: 1)), Color(#colorLiteral(red: 0.3647058904, green: 0.06666667014, blue: 0.9686274529, alpha: 1)), Color(#colorLiteral(red: 0.5843137503, green: 0.8235294223, blue: 0.4196078479, alpha: 1)),Color(#colorLiteral(red: 0.5738074183, green: 0.5655357838, blue: 0, alpha: 1)), Color(#colorLiteral(red: 0, green: 0.5690457821, blue: 0.5746168494, alpha: 1)), Color(#colorLiteral(red: 1, green: 0.2527923882, blue: 1, alpha: 1)), Color(#colorLiteral(red: 0.7254902124, green: 0.4784313738, blue: 0.09803921729, alpha: 1)), Color(#colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1))]
    
    func sliceRecipes(in frame: CGRect) -> [SliceRecipeProviding] {
        let centerPoint = CGPoint(x: frame.midX, y: frame.midY)
        let radius = min(frame.width, frame.height) / 2
        
        var recipes = [SliceRecipeProviding]()
        var tempEndAngle = 0.0
        for (index, percentage) in percentages.enumerated() {
            let startAngle = tempEndAngle
            let endAngle = self.endAngle(for: percentage, startingFrom: startAngle)
            recipes.append(
                SliceRecipe(startAngle: startAngle, endAngle: endAngle, centerPoint: centerPoint, radius: radius, percentage: percentage, color: colors[index])
            )
            tempEndAngle = endAngle
        }
        
        return recipes
    }
    
    private func endAngle(for percentage: Double, startingFrom startAngle: Double) -> Double {
        return startAngle + (percentage * 360 / 100)
    }
}
