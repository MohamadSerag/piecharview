
func bakePie(with percentages: [Double]) -> PieChart {
	PieChart(chef: PieChef(percentages: percentages))
}
